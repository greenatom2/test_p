[hello-devops.site](https://hello-devops.site)

[hello-devops.site/test](https://hello-devops.site/test/)

[hello-devops.site/dev](https://hello-devops.site/dev/)


**Сборка:** `docker build . -t tag_name`

**Пушим:**
       - `docker tag tag_name repos:tag` #вместо repos - путь к реджистри, вместо tag_name - необходимый tag
       - `docker push repos:tag_name`
      
**Запуск:** `docker-compose up -d` #в каталоге ~/certs должны быть tls сертификаты, иначе контейнер не запуститься
