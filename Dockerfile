FROM nginx:alpine
ARG WWW_PATH /usr/share/nginx/html/
EXPOSE 80
EXPOSE 443
COPY ./data/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./data/nginx/app.conf /etc/nginx/conf.d/app.conf
COPY ./data/nginx/bulma-rtl.min.css ./data/nginx/index.html ${WWW_PATH}
